# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################
'''
     joint: 6,
            viscous body,
            3, #<node_1_label>
            position, reference, node, null, 
            linear viscous generic, 
            1.0e3, 0.0, 0.0, 0.0, 0.0, 0.0, 
            0.0, 1.0e4, 0.0, 0.0, 0.0, 0.0, 
            0.0, 0.0, 1.0e4, 0.0, 0.0, 0.0, 
            0.0, 0.0, 0.0, 1.0e4, 0.0, 0.0, 
            0.0, 0.0, 0.0, 0.0, 1.0e4, 0.0, 
            0.0, 0.0, 0.0, 0.0, 0.0, 1.0e4;
'''

import FreeCAD
import Draft

class ViscousBody:
    def __init__(self, obj, label, node, law):
        
        obj.addExtension("App::GroupExtensionPython")
              
        x = node.absolute_position_X
        y = node.absolute_position_Y
        z = node.absolute_position_Z
                              
        #Create scripted object:
        obj.addProperty("App::PropertyString","label","viscous body","label",1).label = label
        obj.addProperty("App::PropertyString","joint","viscous body","joint",1).joint = 'viscous body'
        obj.addProperty("App::PropertyString","node label","viscous body","node label",1).node_label = node.label
        
        obj.addProperty("App::PropertyDistance","absolute_pin_position_X","Absolute pin position","absolute_pin_position_X",1).absolute_pin_position_X = x
        obj.addProperty("App::PropertyDistance","absolute_pin_position_Y","Absolute pin position","absolute_pin_position_Y",1).absolute_pin_position_Y = y
        obj.addProperty("App::PropertyDistance","absolute_pin_position_Z","Absolute pin position","absolute_pin_position_Z",1).absolute_pin_position_Z = z
        
        obj.addProperty("App::PropertyString","constitutive law","viscous body","constitutive law").constitutive_law = law
        
        #Animation parameters:
        obj.addProperty("App::PropertyEnumeration","animate","animation","animate")
        obj.animate=['false','true']

        obj.addProperty("App::PropertyEnumeration","frame","animation","frame")
        obj.frame=['global','local']        
        
        obj.addProperty("App::PropertyFloat","force vector multiplier","Animation","force vector multiplier").force_vector_multiplier = 1.0
                 
        #Add the vector to visualize reaction forces
        Llength = FreeCAD.Units.Quantity(FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/4,FreeCAD.Units.Unit('mm'))
        p1 = FreeCAD.Vector(x, y, z)
        p2 = FreeCAD.Vector(x+Llength, y+Llength, z+Llength)         
        d = Draft.makeLine(p1, p2)
        d.ViewObject.LineColor = (1.00,0.00,0.00)
        d.ViewObject.PointColor = (1.00,0.00,0.00)  
        d.ViewObject.LineWidth = 1.00
        d.ViewObject.PointSize = 1.00
        d.ViewObject.EndArrow = True
        d.ViewObject.ArrowType = u"Arrow"
        d.ViewObject.ArrowSize = str(Llength/75)#+' mm'
        d.ViewObject.Selectable = False
        d.Label = "jf: "+ label
        
        obj.Proxy = self
                         
    def execute(self, fp):   
        JF = FreeCAD.ActiveDocument.getObjectsByLabel("jf: "+fp.label)[0]
        Llength = FreeCAD.Units.Quantity(FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/4,FreeCAD.Units.Unit('mm'))
        node1 = FreeCAD.ActiveDocument.getObjectsByLabel("structural: " + fp.node_label)[0]
        
        x = node1.absolute_position_X
        y = node1.absolute_position_Y
        z = node1.absolute_position_Z
        
        fp.absolute_pin_position_X = x
        fp.absolute_pin_position_Y = y
        fp.absolute_pin_position_Z = z 
        
        JF.Start =  FreeCAD.Vector(x, y, z)
        JF.End = FreeCAD.Vector(x+Llength, y+Llength, z+Llength) 
        
        FreeCAD.Console.PrintMessage("VISCOUS BODY JOINT: " +fp.label+" successful recomputation...\n")                 