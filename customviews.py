import os

class DistanceCustomView():
	def __init__(self, obj):
		obj.Proxy = self

	def getIcon(self):
	        return os.path.join(os.path.dirname(__file__), "icons", "distance.png") 

class SliderCustomView():
	def __init__(self, obj):
		obj.Proxy = self

	def getIcon(self):
	        return os.path.join(os.path.dirname(__file__), "icons", "slider.png") 

class CylindricalCustomView():
	def __init__(self, obj):
		obj.Proxy = self

	def getIcon(self):
	        return os.path.join(os.path.dirname(__file__), "icons", "cylindrical.png") 

class LockCustomView():
	def __init__(self, obj):
		obj.Proxy = self

	def getIcon(self):
	        return os.path.join(os.path.dirname(__file__), "icons", "lock.png") 

class TotalCustomView():
	def __init__(self, obj):
		obj.Proxy = self

	def getIcon(self):
	        return os.path.join(os.path.dirname(__file__), "icons", "total.png") 

class GenelClampCustomView():
	def __init__(self, obj):
		obj.Proxy = self

	def getIcon(self):
	        return os.path.join(os.path.dirname(__file__), "icons", "genelclamp.png") 

class TotalPinCustomView():
	def __init__(self, obj):
		obj.Proxy = self

	def getIcon(self):
	        return os.path.join(os.path.dirname(__file__), "icons", "totalpin.png") 
        
class RevoluteRotationCustomView():
	def __init__(self, obj):
		obj.Proxy = self

	def getIcon(self):
	        return os.path.join(os.path.dirname(__file__), "icons", "revoluterotation.png") 

class DeformableDisplacementCustomView():
	def __init__(self, obj):
		obj.Proxy = self

	def getIcon(self):
	        return os.path.join(os.path.dirname(__file__), "icons", "spring.png") 

class PrismaticCustomView():
	def __init__(self, obj):
		obj.Proxy = self

	def getIcon(self):
	        return os.path.join(os.path.dirname(__file__), "icons", "prismatic.png") 
         
class DynamicNodeCustomView():
	def __init__(self, obj):
         obj.addExtension("Gui::ViewProviderGroupExtensionPython")
         obj.Proxy = self

	def getIcon(self):
         return os.path.join(os.path.dirname(__file__), "icons", "StructuralDynamic.png") 

class AbstractNodeCustomView():
	def __init__(self, obj):
         obj.Proxy = self

	def getIcon(self):
         return os.path.join(os.path.dirname(__file__), "icons", "abstract.png")

class ViscousBodyCustomView():
	def __init__(self, obj):
         obj.Proxy = self

	def getIcon(self):
         return os.path.join(os.path.dirname(__file__), "icons", "honey.png")

class Beam3CustomView():
	def __init__(self, obj):
         obj.Proxy = self

	def getIcon(self):
         return os.path.join(os.path.dirname(__file__), "icons", "beam3.png")
         
class DummyNodeCustomView():
    def __init__(self, obj):        
        obj.addExtension("Gui::ViewProviderGroupExtensionPython")
        obj.Proxy = self

    def getIcon(self):
	        return os.path.join(os.path.dirname(__file__), "icons", "StructuralDummy.png")
         
class StaticNodeCustomView():
    def __init__(self, obj):
        obj.addExtension("Gui::ViewProviderGroupExtensionPython")
        obj.Proxy = self

    def getIcon(self):
	        return os.path.join(os.path.dirname(__file__), "icons", "StructuralStatic.png") 

class RigidBodyCustomView():
	def __init__(self, obj):
		obj.Proxy = self

	def getIcon(self):
	        return os.path.join(os.path.dirname(__file__), "icons", "viga.png") 

class FlexibleBodyCustomView():
	def __init__(self, obj):
		obj.Proxy = self

	def getIcon(self):
	        return os.path.join(os.path.dirname(__file__), "icons", "torsion.png") 

class GearCustomView():
	def __init__(self, obj):
		obj.Proxy = self

	def getIcon(self):
	        return os.path.join(os.path.dirname(__file__), "icons", "gear.png")             

class GearsCustomView():
	def __init__(self, obj):
		obj.Proxy = self

	def getIcon(self):
	        return os.path.join(os.path.dirname(__file__), "icons", "gears.png")   

class DummyBodyCustomView():
	def __init__(self, obj):
		obj.Proxy = self

	def getIcon(self):
	        return os.path.join(os.path.dirname(__file__), "icons", "viga1.png") 

class StaticBodyCustomView():
	def __init__(self, obj):
		obj.Proxy = self

	def getIcon(self):
	        return os.path.join(os.path.dirname(__file__), "icons", "viga2.png") 
            
class RevolutePinCustomView():
	def __init__(self, obj):
		obj.Proxy = self

	def getIcon(self):
	        return os.path.join(os.path.dirname(__file__), "icons", "hinge.png") 
         
class RevoluteHingeCustomView():
	def __init__(self, obj):
		obj.Proxy = self

	def getIcon(self):
	        return os.path.join(os.path.dirname(__file__), "icons", "hinge1.png") 

class ClampCustomView():
	def __init__(self, obj):
		obj.Proxy = self

	def getIcon(self):
	        return os.path.join(os.path.dirname(__file__), "icons", "clamp.png") 

class InLineCustomView():
	def __init__(self, obj):
		obj.Proxy = self

	def getIcon(self):
	        return os.path.join(os.path.dirname(__file__), "icons", "in-line.png") 

class InPlaneCustomView():
	def __init__(self, obj):
		obj.Proxy = self

	def getIcon(self):
	        return os.path.join(os.path.dirname(__file__), "icons", "in-plane.png") 

class AxialCustomView():
	def __init__(self, obj):
		obj.Proxy = self

	def getIcon(self):
	        return os.path.join(os.path.dirname(__file__), "icons", "axial.png")          
         
class StructuralForceCustomView():
	def __init__(self, obj):
		obj.Proxy = self

	def getIcon(self):
	        return os.path.join(os.path.dirname(__file__), "icons", "force.png")  
         
class StructuralCoupleCustomView():
	def __init__(self, obj):
		obj.Proxy = self

	def getIcon(self):
	        return os.path.join(os.path.dirname(__file__), "icons", "couple.png")  
         
class DriveHingeCustomView():
	def __init__(self, obj):
		obj.Proxy = self

	def getIcon(self):
	        return os.path.join(os.path.dirname(__file__), "icons", "drivehinge.png")      
         
class SphericalCustomView():
	def __init__(self, obj):
		obj.Proxy = self

	def getIcon(self):
	        return os.path.join(os.path.dirname(__file__), "icons", "spherical.png")
        
class ImposedDisplacementView():
	def __init__(self, obj):
		obj.Proxy = self

	def getIcon(self):
	        return os.path.join(os.path.dirname(__file__), "icons", "imposed-displacement.png")        